FROM docker:latest

RUN apk add --update nodejs npm

COPY --from=docker/buildx-bin /buildx /usr/libexec/docker/cli-plugins/docker-buildx
